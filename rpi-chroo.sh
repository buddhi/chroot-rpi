#!/bin/bash

##rpi mount point
##change this path according to your mount point 
##or else shit will happen
RPIMOU="/media/olenepal/rootfs"

# mount binds
mount --bind /dev $RPIMOU/dev/
mount --bind /sys $RPIMOU/sys/
mount --bind /proc $RPIMOU/proc/
mount --bind /dev/pts $RPIMOU/dev/pts

# ld.so.preload fix
sed -i 's/^/#CHROOT /g' $RPIMOU/etc/ld.so.preload

# copy qemu binary
cp /usr/bin/qemu-arm-static $RPIMOU/usr/bin/

echo "You will be transferred to the bash shell now."
echo "Issue 'exit' when you are done."
echo "Issue 'su pi' if you need to work as the user pi."

# chroot to raspbian
chroot $RPIMOU /bin/bash