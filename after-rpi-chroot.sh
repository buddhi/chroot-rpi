#!/bin/bash

RPIMOU="/media/olenepal/rootfs"

# Clean up
# revert ld.so.preload fix
sed -i 's/^#CHROOT //g' $RPIMOU/etc/ld.so.preload

# unmount everything
umount $RPIMOU/{dev/pts,dev,sys,proc,boot,}
## not necessary to mount boot partation, if stderr boot show no problem